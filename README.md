datalife_sale_line_standalone_view
=============================

The sale_line_standalone_view module of the Tryton application platform.

[![Build Status](http://drone.datalifeit.es:8050/api/badges/datalifeit/trytond-sale_line_standalone_view/status.svg)](http://drone.datalifeit.es:8050/datalifeit/trytond-sale_line_standalone_view)

Installing
----------

See INSTALL


License
-------

See LICENSE

Copyright
---------

See COPYRIGHT
