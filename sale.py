# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import PoolMeta, Pool
from trytond.model import fields

__all__ = ['SaleLine']


class SaleLine(metaclass=PoolMeta):
    __name__ = 'sale.line'

    sale_date = fields.Function(fields.Date('Sale Date'),
        'get_sale_field', searcher='search_sale_field')
    party = fields.Function(fields.Many2One('party.party', 'Party'),
        'get_sale_field', searcher='search_sale_field')

    @classmethod
    def __setup__(cls):
        super(SaleLine, cls).__setup__()
        for field_name in ('warehouse', 'sale_state'):
            getattr(cls, field_name).searcher = 'search_sale_field'

    @classmethod
    def get_sale_field(cls, records, name=None):
        pool = Pool()
        Sale = pool.get('sale.sale')

        if type(getattr(Sale, name, None)) == fields.Many2One:
            return {r.id: getattr(r.sale, name).id for r in records}
        else:
            return {r.id: getattr(r.sale, name) for r in records}

    @classmethod
    def search_sale_field(cls, name, clause):
        _clause = clause[0]
        if name == 'sale_state':
            _clause = _clause.replace('sale_state', 'state')
        return [('sale.' + _clause, ) + tuple(clause[1:])]

    def _order_sale_field(name):
        def order_field(tables):
            pool = Pool()
            Sale = pool.get('sale.sale')
            field = Sale._fields[name]
            table, _ = tables[None]
            sale_tables = tables.get('sale')
            if sale_tables is None:
                sale = Sale.__table__()
                sale_tables = {
                    None: (sale, sale.id == table.sale),
                    }
                tables['sale'] = sale_tables
            return field.convert_order(name, sale_tables, Sale)
        return staticmethod(order_field)

    order_sale_date = _order_sale_field('sale_date')
    order_party = _order_sale_field('party')
    order_sale_state = _order_sale_field('state')
